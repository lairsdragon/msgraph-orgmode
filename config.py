"""Configuration settings for console app using device flow authentication
"""

import os
import configparser


# Read Config from File
config = configparser.RawConfigParser({
    'client_id': 'ENTER_YOUT_CLIENT_ID',
    'authority_url': 'https://login.microsoftonline.com/common',
    'resource': 'https://graph.microsoft.com',
    'api_version': 'v1.0',
    'username': '',
    'password': '',
    'days_history': 7,
    'days_future': 30,
    'max_entries': 100
})
dir = os.path.dirname(os.path.realpath(__file__))
config.read(os.path.join(dir, 'config.cfg'))

CLIENT_ID = config.get('msgraph-orgmode', 'client_id')
AUTHORITY_URL = config.get('msgraph-orgmode', 'authority_url')
RESOURCE = config.get('msgraph-orgmode', 'resource')
API_VERSION = config.get('msgraph-orgmode', 'api_version')

username = config.get('msgraph-orgmode', 'username')
password = config.get('msgraph-orgmode', 'password')

daysHistory = config.getint('msgraph-orgmode', 'days_history')
daysFuture = config.getint('msgraph-orgmode', 'days_future')
maxEntries = config.getint('msgraph-orgmode', 'max_entries')

if 'ENTER_YOUR' in CLIENT_ID:
    print('ERROR: config.py does not contain valid CLIENT_ID.')
    import sys
    sys.exit(1)
