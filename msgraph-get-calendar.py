#
#
#
# based on sample code of Microsoft
#  - https://github.com/microsoftgraph/python-sample-console-app
# and ews-orgmode
#  - https://github.com/kautsig/ews-orgmode
#
#
# More Info for MS Graph
# - https://developer.microsoft.com/en-us/graph/graph-explorer
#


import pprint
import urllib
import string
from datetime import datetime
from datetime import date
from datetime import timedelta

import pytz

from adal import AuthenticationContext

import requests

import config

def api_endpoint(url):
    """Convert a relative path such as /me/photo/$value to a full URI based
    on the current RESOURCE and API_VERSION settings in config.py.
    """
    if urllib.parse.urlparse(url).scheme in ['http', 'https']:
        return url # url is already complete
    return urllib.parse.urljoin(f'{config.RESOURCE}/{config.API_VERSION}/',
                                url.lstrip('/'))

def user_password_session(client_id, username, password):
    """Obtain an access token from Azure AD (via username and password)
    and create a Requests session instance ready to make authenticated
    calls to Microsoft Graph.

    client_id = Application ID for registered "Azure AD only" V1-endpoint app
    username  = username
    password  = password of user

    Returns Requests session object if user signed in successfully. The session
    includes the access token in an Authorization header.

    User identity must be an organizational account (ADAL does not support MSAs).

    """
    ctx = AuthenticationContext(config.AUTHORITY_URL, api_version=None)
    token_response = ctx.acquire_token_with_username_password(config.RESOURCE,
                                                           username,
                                                           password,
                                                           client_id,)

    if not token_response.get('accessToken', None):
        return None

    # pprint.pprint(token_response)
    
    session = requests.Session()
    session.headers.update({'Authorization': f'Bearer {token_response["accessToken"]}',
                            'SdkVersion': 'sample-python-adal',
                            'x-client-SKU': 'sample-python-adal'})
    return session


def parse_cal_date(dateStr):
    d = datetime.strptime(dateStr, "%Y-%m-%dT%H:%M:%S.0000000")
    exchangeTz = pytz.utc
    localTz = pytz.timezone('MET')
    return exchangeTz.localize(d).astimezone(localTz);


def format_orgmode_date(dateObj):
  return dateObj.strftime("%Y-%m-%d %H:%M")


def format_orgmode_time(dateObj):
  return dateObj.strftime("%H:%M")


def get_calendar(session):
    """Get Calendar from O365
    """

    start = date.today() - timedelta(days=config.daysHistory)
    end = date.today() + timedelta(days=config.daysFuture)

    cal = session.get(api_endpoint(f"me/calendarview?startdatetime={start}T00:00:00.000Z&enddatetime={end}T23:59:59.999Z&$top={config.maxEntries}&$orderby=start/dateTime"))

    calentries = cal.json()
    # pprint.pprint(cal.json())
    for appt in calentries['value']:
        # pprint.pprint(appt)
        
        apptstart = parse_cal_date(appt['start']['dateTime'])
        apptend = parse_cal_date(appt['end']['dateTime'])
        tags = ""
        if appt['categories']:
            tags = ":" + ":".join(appt['categories']) + ":"
        else:
            tags = ":WORK:"
            
        if apptstart.date() == apptend.date():
            dateStr = "<" +  format_orgmode_date(apptstart) + "-" + format_orgmode_time(apptend) + ">"
        else:
            dateStr = "<" +  format_orgmode_date(apptstart) + ">--<" + format_orgmode_date(apptend) + ">"
        body = appt['bodyPreview'].translate({ord('\r'): None})
        
        print(f"* {dateStr} {appt['subject']} {tags}")

        print(":PROPERTIES:")
        if appt['location']['displayName'] is not None:
            print(f":LOCATION: {appt['location']['displayName']}")
        
        if appt['onlineMeeting'] is not None:
            print(f":JOINURL: {appt['onlineMeeting']['joinUrl']}")

        print(f":RESPONSE: {appt['responseStatus']['response']}")
        print(":END:")

        print(f"{body}")

        print("")


if __name__ == '__main__':
    #GRAPH_SESSION = device_flow_session(config.CLIENT_ID)
    GRAPH_SESSION = user_password_session(config.CLIENT_ID,config.username,config.password)
    if GRAPH_SESSION:
#        pprint.pprint(GRAPH_SESSION)
        get_calendar(GRAPH_SESSION)
